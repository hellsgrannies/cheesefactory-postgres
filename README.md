# cheesefactory-postgres

-----------------

#### A simple PostgreSQL wrapper for psycopg2.
[![PyPI Latest Release](https://img.shields.io/pypi/v/cheesefactory-postgres.svg)](https://pypi.org/project/cheesefactory-postgres/)
[![PyPI status](https://img.shields.io/pypi/status/cheesefactory-postgres.svg)](https://pypi.python.org/pypi/cheesefactory-postgres/)
[![PyPI download month](https://img.shields.io/pypi/dm/cheesefactory-postgres.svg)](https://pypi.python.org/pypi/cheesefactory-postgres/)
[![PyPI download week](https://img.shields.io/pypi/dw/cheesefactory-postgres.svg)](https://pypi.python.org/pypi/cheesefactory-postgres/)
[![PyPI download day](https://img.shields.io/pypi/dd/cheesefactory-postgres.svg)](https://pypi.python.org/pypi/cheesefactory-postgres/)

### Main Features

**Note:** _This package is still in beta status. As such, future versions may not be backwards compatible and features may change. Parts of it may even be broken._

Coming soon.
